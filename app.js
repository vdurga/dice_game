/*
GAME RULES:

- The game has 2 players, playing in rounds
- In each turn, a player rolls a dice as many times as he whishes. Each result get added to his ROUND score
- BUT, if the player rolls a 1, all his ROUND score gets lost. After that, it's the next player's turn
- The player can choose to 'Hold', which means that his ROUND score gets added to his GLBAL score. After that, it's the next player's turn
- The first player to reach 100 points on GLOBAL score wins the game

*/

/***** ROLL DICE

	1. Create an event (click) Roll Dice 
	2. Declare a Variable dice
	3. Give randoom values to dice
	4. Add dice src
	5. save the value to the current button (totalScore +activescore)
	6. If dice = 1 then  cahnge the current player
	7. When dice changes to next player disable the dice

*****/
var totalScore, activePlayer, roundScore, gameWinner;
int();

document.querySelector('.btn-roll').addEventListener('click', function(){
	if(gameWinner){
	var dice;
	dice = Math.floor(Math.random() * 6 ) + 1;
	document.querySelector('.dice').style.display = 'block';
	document.querySelector('.dice').src = 'dice-' + dice + '.png'; 
	//console.log(dice);
	if( dice !== 1 )
	{
		totalScore += dice;
		document.querySelector('#current-' + activePlayer).textContent = totalScore;
	}else{
		aciveplayer();
	}
	}
});


/***** HOLD

	1.Create an event (click) Hold
	
*****/

document.querySelector('.btn-hold').addEventListener('click', function(){
	if (gameWinner){
		roundScore[activePlayer] += totalScore ;
		document.querySelector('#score-' + activePlayer).textContent =  roundScore[activePlayer];
		if ( roundScore[activePlayer] >= 20)
		{
			document.querySelector('#name-'+ activePlayer).textContent = 'Winner !'
			gameWinner = false;
		}
		else{
			aciveplayer();
		}
	}
});


/***** NEW GAME

	1.Create an event (click) New Game
	
*****/
document.querySelector('.btn-new').addEventListener('click', int);

function aciveplayer()
{
	document.querySelector('.dice').style.display = 'none';
	totalScore = 0;
	document.querySelector('#current-' + activePlayer).textContent = 0;
	activePlayer === 0 ? activePlayer = 1: activePlayer =0;
	//document.querySelector('.player-0-panel').classList.toggle('active');
	//document.querySelector('.player-1-panel').classList.toggle('active');
}

function int()
{
	totalScore = 0;
	activePlayer = 0;
	roundScore = [0,0];
	gameWinner = true;
	document.querySelector('.dice').style.display = 'none';
	document.querySelector('#score-0').textContent =0;
	document.querySelector('#score-1').textContent =0;
	document.querySelector('#current-0').textContent = 0;
	document.querySelector('#current-1').textContent = 0;
	document.querySelector('#name-0').textContent = 'Player 1';
	document.querySelector('#name-1').textContent = 'Player 2';
	document.querySelector('.player-0-panel').classList.remove('active');
	document.querySelector('.player-1-panel').classList.remove('active');
	document.querySelector('.player-0-panel').classList.add('active');
}